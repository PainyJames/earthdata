dataSource {
    pooled = true
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
	driverClassName = "com.mysql.jdbc.Driver"
    username = "JMcerezas"
    password = "Abril2008"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
	cache.provider_class = 'org.hibernate.cache.EhCacheProvider'
}
// environment specific settings
environments {
    development {
        dataSource {
			dbCreate = "" // one of 'create', 'create-drop', 'update', 'validate', ''
			url = "jdbc:mysql://146.255.102.176:3306/earthdata"
        }
    }
    test {
        dataSource {
            dbCreate = ""
			url = "jdbc:mysql://146.255.102.176:3306/earthdata"
        }
    }
    production {
        dataSource {
            dbCreate = ""
			url = "jdbc:mysql://146.255.102.176:3306/earthdata"
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
