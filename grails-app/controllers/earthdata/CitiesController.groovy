package earthdata

import earthdata.domain.Cities
import earthdata.domain.Regions
import grails.converters.JSON

class CitiesController {

    def index() { render '' }
	
	def regionCities()
	{
		def result = Cities.findAllByRegionIdRegions(Regions.get(params.int('id'))) .collect
		{
			[cityId:it.cityId, cityName: it.cityName]
		}
		render result as JSON
	}
}
