package earthdata

import earthdata.domain.Countries
import grails.converters.JSON;

class CountriesController {

	static defaultAction = 'all'
	
    def all() { 
		def result = Countries.getAll().collect
		{
			[id:it.id, countryName:it.countryName, countryCode: it.countryCode, countryCallCode: it.countryCallCode]
		}
		render result as JSON		
	}
	
}
