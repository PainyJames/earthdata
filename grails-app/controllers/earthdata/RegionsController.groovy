package earthdata

import grails.converters.JSON
import earthdata.domain.Regions
import earthdata.domain.Countries

class RegionsController {

    def index() { render '' }
	
	def countryRegions()
	{
		def result = Regions.findAllByCountryIdCountries(Countries.get(params.int('id'))) .collect
		{
			[regionId:it.regionId, regionName: it.regionName]
		}
		render result as JSON
	}
}
