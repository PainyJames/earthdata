package earthdata.domain

/**
 * The Cities entity.
 *
 * @author    
 *
 *
 */
class Cities {
    static mapping = {
         table 'cities'
         // version is set to false, because this isn't available by default for legacy databases
         version false
         id generator:'identity', column:'city_id'
         regionIdRegions column:'region_id'
		 cityId insertable: false
		 cityId updateable: false
    }
    int cityId
    String cityName
    // Relation
    Regions regionIdRegions

    static constraints = {
        cityName(size: 0..100)
        regionIdRegions()
    }
    String toString() {
        return "${cityId}" 
    }
}
