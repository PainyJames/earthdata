package earthdata.domain
/**
 * The Countries entity.
 *
 * @author    
 *
 *
 */
class Countries {
    static mapping = {
         table 'countries'
         // version is set to false, because this isn't available by default for legacy databases
         version false
         id generator:'identity', column:'country_id'
		 countryId insertable: false
		 countryId updateable: false
    }
    int countryId
    String countryCode
    String countryName
    int countryCallCode

    static constraints = {
        countryCode(size: 0..2)
        countryName(size: 0..100)
        countryCallCode(nullable: true)
    }
    String toString() {
        return "${countryId}" 
    }
}
