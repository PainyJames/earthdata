package earthdata.domain

/**
 * The Regions entity.
 *
 * @author    
 *
 *
 */
class Regions {
    static mapping = {
         table 'regions'
         // version is set to false, because this isn't available by default for legacy databases
         version false
         id generator:'identity', column:'region_id'
         countryIdCountries column:'country_id'
		 regionId insertable: false
		 regionId updateable: false
    }
    int regionId
    String regionName
    // Relation
    Countries countryIdCountries

    static constraints = {
        regionName(size: 0..100)
        countryIdCountries()
    }
    String toString() {
        return "${regionId}" 
    }
}
